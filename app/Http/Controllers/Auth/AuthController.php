<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Foundation\Auth\ThrottlesLogins;
// use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\View;
use Validator;

class AuthController extends Controller
{
    protected $loginPath;
    protected $redirectPath;
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
     */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'getLogout']);
        $this->loginPath    = URL::to('/') . '/login';
        $this->redirectPath = URL::route('admin.index');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name'     => 'required|max:255',
            'email'    => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name'     => $data['name'],
            'email'    => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

    public function getLogin()
    {
        $rangking = null;
        if (Storage::exists('rangking.json')) {
            $rangking = Storage::get('rangking.json');
        }
        if ($rangking !== null) {
            $rangkings = explode(PHP_EOL, $rangking);
            // return $rangkings;
            foreach ($rangkings as $r) {
                if (!empty($r)) {
                    $data    = explode('|', $r);
                    $ranks[] = [
                        'name' => $data[0],
                        'rank' => $data[1],
                    ];
                }
            }
            $rangking = $ranks;
        }
        $data = [
            'rangking' => $rangking,
        ];
        return View::make('admin.template.user.login', $data)->with(array(
            'title'  => 'Login',
            'noAuth' => true,
        ));
    }

    // public function postLogin(Request $request)
    // {
    //     // return $request->all();
    // }
}
