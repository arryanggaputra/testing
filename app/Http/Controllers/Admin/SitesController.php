<?php

namespace App\Http\Controllers\Admin;

use App\Helper\Result;
use App\Http\Controllers\Controller;
use App\Models\Site;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\View;

class SitesController extends Controller
{
    public function __construct()
    {
        $this->result = new Result;
        View::share('groupPage', 'sites');
    }

    public function index(Request $request)
    {
        $no    = (empty($request->get('page'))) ? 1 : $request->get('page');
        $no    = ($no - 1) * $this->paginate();
        $sites = Site::orderBy('id', 'DESC');
        if ($request->get('q')) {
            $sites->where('name', 'LIKE', '%' . $request->get('q') . '%');
        }
        $data = [
            'sites' => $sites->paginate($this->paginate()),
            'no'    => $no,
        ];
        return View::make('admin.sites.index', $data)->with(array(
            'title' => 'Daftar Situs Web',
        ));
    }

    public function create()
    {
        return View::make('admin.sites.create')->with(array(
            'title' => 'Buat Situs Web Baru',
        ));
    }

    public function show($id)
    {
        $sites   = Site::find($id);
        $content = file_get_contents($sites->url . '/wp-json/wp/v2/posts');
        $content = json_decode($content);
        $data    = [
            'sites'    => $sites,
            'contents' => $content,
        ];
        return View::make('admin.sites.show', $data)->with(array(
            'title' => $sites->name,
        ));
    }
    public function getRead($id, $id_tulisan)
    {
        $sites   = Site::find($id);
        $content = file_get_contents($sites->url . '/wp-json/wp/v2/posts/' . $id_tulisan);
        $content = json_decode($content);
        $data    = [
            'sites'    => $sites,
            'contents' => $content,
        ];
        return View::make('admin.sites.read', $data)->with(array(
            'title' => $sites->name,
        ));
    }

    public function getWrite($id)
    {
        $sites = Site::find($id);
        $data  = [
            'sites' => $sites,
        ];
        return View::make('admin.sites.write', $data)->with(array(
            'title' => $sites->name,
        ));
    }

    public function postWrite(Request $request, $id)
    {
        $sites = Site::find($id);
        $data  = [
            'sites' => $sites,
        ];
        return $request->all();
        return View::make('admin.sites.write', $data)->with(array(
            'title' => $sites->name,
        ));
    }

    public function edit(Request $request, $id)
    {
        $sites = Site::find($id);
        if (empty($sites)) {
            return App::abort('404');
        }
        $data = [
            'sites' => $sites,
        ];
        return View::make('admin.sites.create', $data)->with(array(
            'title'     => 'Ubah Situs Web ',
            'edit_data' => true,
        ));
    }

    public function store(Request $request)
    {
        $req   = $request->all();
        $sites = Site::manageData(null, $req);
        if ($sites['status'] == 0) {
            return $this->result->make($sites['data'], null, 400);
        } else {
            return $this->result->make(
                $sites['data'],
                URL::route('admin.sites.index') . '?success=' . $sites['data']['name'] . ' telah ditambahkan',
                200
            );
        }
    }

    public function update(Request $request, $id)
    {
        $req   = $request->all();
        $sites = Site::manageData($id, $req);
        if ($sites['status'] == 0) {
            return $this->result->make($sites['data'], null, 400);
        } else {
            return $this->result->make(
                $sites['data'],
                URL::route('admin.sites.index') . '?success=' . $sites['data']['name'] . ' telah diperbaharui',
                200
            );
        }
    }

    // public function deleting($id)
    // {
    //     $data = User::where('type', 3)->find($id);
    //     if (empty($data)) {
    //         return App::abort(404);
    //     }
    //     $data->delete();
    //     return Redirect::route('admin.sites.index')->with('error', $data['name'] . ' telah dihapus');
    // }
}
