<?php

namespace App\Http\Controllers\Admin;

use App\Helper\Result;
use App\Http\Controllers\Controller;
use App\Models\City;
use App\Models\Province;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\View;

class SuperadminsController extends Controller
{
    public function __construct()
    {

        View::share('groupPage', 'superadmins');
        $this->result = new Result;
        if (Auth::user()->type !== 1) {
            return App::abort(404);
        }
    }

    public function index(Request $request)
    {
        $no          = (empty($request->get('page'))) ? 1 : $request->get('page');
        $no          = ($no - 1) * $this->paginate();
        $superadmins = User::with('city')->where('type', 1)->orderBy('id', 'DESC');

        if ($request->get('q')) {
            $superadmins->where('name', 'LIKE', '%' . $request->get('q') . '%');
        }
        if ($request->get('city_id')) {
            $superadmins->where('city_id', $request->get('city_id'));
        }

        $data = [
            'superadmins' => $superadmins->paginate($this->paginate()),
            'no'          => $no,
        ];
        return View::make('admin.superadmins.index', $data)->with(array(
            'title' => 'Daftar Super Administrator',
        ));
    }

    public function create()
    {
        $province = new Province;
        $data     = array(
            'provinces' => $province->getSelectList(),
        );
        return View::make('admin.superadmins.create', $data)->with(array(
            'title' => 'Buat Super Administrator Baru',
        ));
    }

    public function edit(Request $request, $id)
    {
        $superadmins = User::where('type', 1)->find($id);
        $province    = new Province;
        $city        = new City;
        if (empty($superadmins)) {
            return App::abort('404');
        }
        $data = [
            'superadmins' => $superadmins,
            'provinces'   => $province->getSelectList(),
            'cities'      => $city->getSelectList(($superadmins->city) ? $superadmins->city->province_id : null),
        ];
        return View::make('admin.superadmins.create', $data)->with(array(
            'title'     => 'Ubah Super Administrator ',
            'edit_data' => true,
        ));
    }

    public function store(Request $request)
    {
        $req         = array_merge($request->all(), ['type' => 1]);
        $superadmins = User::manageData(null, $req);
        if ($superadmins['status'] == 0) {
            return $this->result->make($superadmins['data'], null, 400);
        } else {
            return $this->result->make(
                $superadmins['data'],
                URL::route('admin.superadmins.index') . '?success=' . $superadmins['data']['name'] . ' telah ditambahkan',
                200
            );
        }
    }

    public function update(Request $request, $id)
    {
        $req         = array_merge($request->all(), ['type' => 1]);
        $superadmins = User::manageData($id, $req);
        if ($superadmins['status'] == 0) {
            return $this->result->make($superadmins['data'], null, 400);
        } else {
            return $this->result->make(
                $superadmins['data'],
                URL::route('admin.superadmins.index') . '?success=' . $superadmins['data']['name'] . ' telah diperbaharui',
                200
            );
        }
    }

    public function deleting($id)
    {
        $data = User::where('type', 1)->find($id);
        if (empty($data)) {
            return App::abort(404);
        }
        $data->delete();
        return Redirect::route('admin.superadmins.index')->with('error', $data['name'] . ' telah dihapus');
    }
}
