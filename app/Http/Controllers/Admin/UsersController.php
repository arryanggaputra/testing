<?php

namespace App\Http\Controllers\Admin;

use App\Helper\Result;
use App\Http\Controllers\Controller;
use App\Models\City;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\View;

class UsersController extends Controller
{
    public function __construct()
    {
        $this->result = new Result;
        View::share('groupPage', 'users');
    }

    public function index(Request $request)
    {
        $no    = (empty($request->get('page'))) ? 1 : $request->get('page');
        $no    = ($no - 1) * $this->paginate();
        $users = User::where('type', 1)->orderBy('id', 'DESC');

        if ($request->get('q')) {
            $users->where('name', 'LIKE', '%' . $request->get('q') . '%');
        }
        if ($request->get('city_id')) {
            $users->where('city_id', $request->get('city_id'));
        }

        $data = [
            'users' => $users->paginate($this->paginate()),
            'no'    => $no,
        ];
        return View::make('admin.users.index', $data)->with(array(
            'title' => 'Daftar Pengguna',
        ));
    }

    public function create()
    {
        // $province = new Province;
        $data = array(
            // 'provinces' => $province->getSelectList(),
        );
        return View::make('admin.users.create', $data)->with(array(
            'title' => 'Buat Pengguna Baru',
        ));
    }

    public function edit(Request $request, $id)
    {
        $users    = User::where('type', 1)->find($id);
        $province = new Province;
        $city     = new City;
        if (empty($users)) {
            return App::abort('404');
        }
        $data = [
            'users'     => $users,
            'provinces' => $province->getSelectList(),
            'cities'    => $city->getSelectList(($users->city) ? $users->city->province_id : null),
        ];
        return View::make('admin.users.create', $data)->with(array(
            'title'     => 'Ubah Pengguna ',
            'edit_data' => true,
        ));
    }

    public function store(Request $request)
    {
        $req   = array_merge($request->all(), ['type' => 3]);
        $users = User::manageData(null, $req);
        if ($users['status'] == 0) {
            return $this->result->make($users['data'], null, 400);
        } else {
            return $this->result->make(
                $users['data'],
                URL::route('admin.users.index') . '?success=' . $users['data']['name'] . ' telah ditambahkan',
                200
            );
        }
    }

    public function update(Request $request, $id)
    {
        $req   = array_merge($request->all(), ['type' => 3]);
        $users = User::manageData($id, $req);
        if ($users['status'] == 0) {
            return $this->result->make($users['data'], null, 400);
        } else {
            return $this->result->make(
                $users['data'],
                URL::route('admin.users.index') . '?success=' . $users['data']['name'] . ' telah diperbaharui',
                200
            );
        }
    }

    public function deleting($id)
    {
        $data = User::where('type', 1)->find($id);
        if (empty($data)) {
            return App::abort(404);
        }
        $data->delete();
        return Redirect::route('admin.users.index')->with('error', $data['name'] . ' telah dihapus');
    }
}
