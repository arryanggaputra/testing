<?php

namespace App\Http\Controllers\Admin;

use App\Helper\Result;
use App\Http\Controllers\Controller;
use App\Models\City;
use App\Models\Province;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\View;

class AdminsController extends Controller
{
    public function __construct()
    {
        $this->result = new Result;
        View::share('groupPage', 'admins');
    }

    public function index(Request $request)
    {
        $no     = (empty($request->get('page'))) ? 1 : $request->get('page');
        $no     = ($no - 1) * $this->paginate();
        $admins = User::with('city')->where('type', 2)->orderBy('id', 'DESC');

        if ($request->get('q')) {
            $admins->where('name', 'LIKE', '%' . $request->get('q') . '%');
        }
        if ($request->get('city_id')) {
            $admins->where('city_id', $request->get('city_id'));
        }

        $data = [
            'admins' => $admins->paginate($this->paginate()),
            'no'     => $no,
        ];
        return View::make('admin.admins.index', $data)->with(array(
            'title' => 'Daftar Administrator',
        ));
    }

    public function create()
    {
        $province = new Province;
        $data     = array(
            'provinces' => $province->getSelectList(),
        );
        return View::make('admin.admins.create', $data)->with(array(
            'title' => 'Buat Administrator Baru',
        ));
    }

    public function edit(Request $request, $id)
    {
        $admins   = User::where('type', 2)->find($id);
        $province = new Province;
        $city     = new City;
        if (empty($admins)) {
            return App::abort('404');
        }
        $data = [
            'admins'    => $admins,
            'provinces' => $province->getSelectList(),
            'cities'    => $city->getSelectList(($admins->city) ? $admins->city->province_id : null),
        ];
        return View::make('admin.admins.create', $data)->with(array(
            'title'     => 'Ubah Administrator ',
            'edit_data' => true,
        ));
    }

    public function store(Request $request)
    {
        $req    = array_merge($request->all(), ['type' => 2]);
        $admins = User::manageData(null, $req);
        if ($admins['status'] == 0) {
            return $this->result->make($admins['data'], null, 400);
        } else {
            return $this->result->make(
                $admins['data'],
                URL::route('admin.admins.index') . '?success=' . $admins['data']['name'] . ' telah ditambahkan',
                200
            );
        }
    }

    public function update(Request $request, $id)
    {
        $req    = array_merge($request->all(), ['type' => 2]);
        $admins = User::manageData($id, $req);
        if ($admins['status'] == 0) {
            return $this->result->make($admins['data'], null, 400);
        } else {
            return $this->result->make(
                $admins['data'],
                URL::route('admin.admins.index') . '?success=' . $admins['data']['name'] . ' telah diperbaharui',
                200
            );
        }
    }

    public function deleting($id)
    {
        $data = User::where('type', 2)->find($id);
        if (empty($data)) {
            return App::abort(404);
        }
        $data->delete();
        return Redirect::route('admin.admins.index')->with('error', $data['name'] . ' telah dihapus');
    }

    public function rangking()
    {
        $rangking = null;
        if (Storage::exists('rangking.json')) {
            $rangking = Storage::get('rangking.json');
        }
        $data = [
            'rangking' => $rangking,
        ];
        return View::make('admin.rangking', $data)->with(array(
            'title' => 'Atur Rangking Situs',
        ));
    }
    public function postRangking(Request $request)
    {
        $contents = $request->get('rangking');
        Storage::put('rangking.json', $contents);
        return Redirect::back()->with('success', 'Rangking Tribratanews teleh di perbarui');
    }
}
