<?php

namespace App\Http\Controllers\Admin;

use App\Helper\Result;
use App\Http\Controllers\Controller;
use App\Models\Highlight;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\View;

class HighlightsController extends Controller
{
    public function __construct()
    {
        $this->result = new Result;
        View::share('groupPage', 'highlights');
    }

    public function index(Request $request)
    {
        $no         = (empty($request->get('page'))) ? 1 : $request->get('page');
        $no         = ($no - 1) * $this->paginate();
        $highlights = Highlight::orderBy('id', 'DESC');
        if ($request->get('q')) {
            $highlights->where('name', 'LIKE', '%' . $request->get('q') . '%');
        }
        if ($request->get('type')) {
            $highlights->where('type', $request->get('type'));
        }

        $data = [
            'highlights' => $highlights->paginate($this->paginate()),
            'no'         => $no,
        ];
        return View::make('admin.highlights.index', $data)->with(array(
            'title' => 'Daftar Highlight',
        ));
    }

    public function create()
    {
        return View::make('admin.highlights.create')->with(array(
            'title' => 'Buat Highlight Baru',
        ));
    }

    public function edit(Request $request, $id)
    {
        $highlights = Highlight::find($id);
        if (empty($highlights)) {
            return App::abort('404');
        }
        $data = [
            'highlights' => $highlights,
        ];
        return View::make('admin.highlights.create', $data)->with(array(
            'title'     => 'Ubah Highlight ',
            'edit_data' => true,
        ));
    }

    public function store(Request $request)
    {
        $req        = $request->all();
        $highlights = Highlight::manageData(null, $req);
        if ($highlights['status'] == 0) {
            return $this->result->make($highlights['data'], null, 400);
        } else {
            return $this->result->make(
                $highlights['data'],
                URL::route('admin.highlights.index') . '?success=' . $highlights['data']['name'] . ' telah ditambahkan',
                200
            );
        }
    }

    public function update(Request $request, $id)
    {
        $req        = $request->all();
        $highlights = Highlight::manageData($id, $req);
        if ($highlights['status'] == 0) {
            return $this->result->make($highlights['data'], null, 400);
        } else {
            return $this->result->make(
                $highlights['data'],
                URL::route('admin.highlights.index') . '?success=' . $highlights['data']['name'] . ' telah diperbaharui',
                200
            );
        }
    }

    // public function deleting($id)
    // {
    //     $data = User::where('type', 3)->find($id);
    //     if (empty($data)) {
    //         return App::abort(404);
    //     }
    //     $data->delete();
    //     return Redirect::route('admin.highlights.index')->with('error', $data['name'] . ' telah dihapus');
    // }
}
