<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\City;
use App\Models\User;
use Illuminate\Support\Facades\View;

class HomeController extends Controller
{

    public function index()
    {
        return View::make('admin.dashboard.index')->with(array(
            'title'    => 'Dashboard',
            'subtitle' => 'Selamat Datang Sistem Informasi Media Management ',
        ));
    }

    public function cities($id)
    {
        $city = new City;
        $tpl  = "";
        foreach ($city->getSelectList($id) as $c => $name) {
            $tpl = $tpl . '<option value="' . $c . '">' . $name . '</option>';
        }
        return $tpl;
    }
    /**
     * Prepare login interface
     */
    // public function login()
    // {
    //     // echo Form::label('email', 'E-Mail Address');
    //     // exit;
    //     return View::make('admin.template.user.login')->with(array(
    //         'title'  => 'Login',
    //         'noAuth' => true,
    //     ));
    // }

    /**
     * Attempting login post from user
     */
    // public function loginPost()
    // {
    //     $rules = array(
    //         'email'    => 'required',
    //         'password' => 'required|min:3',
    //     );
    //     $validation = Validator::make(Input::all(), $rules);
    //     if ($validation->fails()) {
    //         return Redirect::back()->withErrors($validation)->withInput()->with('error', 'Repeat Please');
    //     } else {
    //         if (Auth::attempt(array('email' => Input::get('email'), 'password' => Input::get('password')))) {
    //             return Redirect::route('admin.homepage')->with('success', 'You are now logged in!');
    //         } else {
    //             return Redirect::back()->with('error', 'Your email/password was incorrect')->withInput();
    //         }
    //     }
    // }

}
