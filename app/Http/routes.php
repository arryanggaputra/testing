<?php
use Illuminate\Support\Facades\Config;

/**
 * ROUTING
 * ---------------------------------------------
 * Admin
 * ---------------------------------------------
 */
Route::get('login', array('as' => 'admin.users.login', 'uses' => 'Auth\AuthController@getLogin'));
Route::post('login', array('as' => 'admin.users.login', 'uses' => 'Auth\AuthController@postLogin'));
Route::get('logout', array('as' => 'admin.users.logout', 'uses' => 'Auth\AuthController@getLogout'));
Route::get('/', array('as' => 'admin.index', 'uses' => 'Admin\HomeController@index', 'middleware' => 'AdminAuth'));
Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => 'AdminAuth'], function () {
    Route::get('/rangking', array('as' => 'admin.rangking', 'uses' => 'AdminsController@rangking'));
    Route::post('/rangking', array('as' => 'admin.rangking', 'uses' => 'AdminsController@postRangking'));
    $prefix = 'admin';
    foreach (Config::get('fileControl.AdminControllers') as $route => $controller) {
        Route::resource($route, $controller);
        Route::get('/' . $route . '/{' . $route . '}/destroy', array('as' => $prefix . '.' . $route . '.deleting', 'uses' => $controller . '@deleting'));
    }
    Route::get('/sites/{id}/write', array('as' => 'admin.sites.write', 'uses' => 'SitesController@getWrite'));
    Route::post('/sites/{id}/write', array('as' => 'admin.sites.write', 'uses' => 'SitesController@postWrite'));
    Route::get('/sites/{id}/{id_tulisan}', array('as' => 'admin.sites.read', 'uses' => 'SitesController@getRead'));

});
