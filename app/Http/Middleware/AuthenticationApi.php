<?php

namespace App\Http\Middleware;

use App\Helper\Result;
use App\Models\User;
use Closure;
use Illuminate\Support\Facades\Auth;

class AuthenticationApi
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function __construct()
    {
        $this->result = new Result;
    }

    public function handle($request, Closure $next)
    {
        $tokens = $request->header('X-Auth-Token');
        $user   = User::where('api_token', $tokens)->first();
        if (!$tokens || !$user) {
            return $this->result->make(null, null, 401);
        }
        Auth::onceUsingId($user->id);
        return $next($request);
    }
}
