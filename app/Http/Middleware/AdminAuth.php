<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;

class AdminAuth
{

    public function handle($request, Closure $next)
    {
        if (!Auth::user() || Auth::user()->type == 3) {
            Auth::logout();
            return redirect(URL::to('/') . '/login')->withErrors('Your are not user');
        }
        return $next($request);
    }
}
