<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class User extends Model implements AuthenticatableContract,
AuthorizableContract,
CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    /**
     * To give information about user status
     * @return [type] [description]
     */
    public function getStatusAttribute()
    {
        switch ($this->type) {
            case '1':
                return "superadmin";
                break;

            case '2':
                return "admin";
                break;

            default:
                return "user";
                break;
        }
    }

    /**
     * Create or Update Database
     *
     * @param  primary key $id if there's no $id this method will create new Data, if preset will be Update the Database
     */
    protected function manageData($id = null, $input = null)
    {
        /**
         * set edit as false
         * @var boolean
         */
        $edit = false;

        /**
         * Check $id
         */
        if (null !== $id) {

            /**
             * set edit as true
             * @var boolean
             */
            $edit = true;

            $getItem = $this->find($id);
            if (empty($getItem)) {
                return $this->result->make();
            }
        }

        if (empty($input)) {
            $input = \Request::all();
        }

        $currentId = (null == $id) ? '' : $id;
        $required  = (null == $id) ? 'required' : '';

        /**
         * set validation on here
         * @var array
         */
        $customrules = [
            'email'    => $required . '|email|max:50|unique:users,email,' . $currentId,
            'phone'    => 'required|max:50|unique:users,phone,' . $currentId,
            'name'     => 'required|max:100',
            'password' => 'confirmed|min:4|' . $required,
        ];

        /**
         * If there's change_password parameter and set as true
         * $customrules will be merge with password validation
         */
        if (!empty($input['change_password'])) {
            if ($input['change_password'] == true) {
                $customrules = array_merge($customrules, [
                    'password' => 'confirmed|min:4|required',
                ]);
            }
        }

        // Create validation include custom rules
        $validation = Validator::make($input, $customrules);

        if ($validation->fails()) {
            $errors = $validation->messages()->all(':message');
            return [
                'status' => 0,
                'data'   => $errors,
            ];
        } else {
            $data       = (null == $id) ? new $this : $getItem;
            $columnList = \Schema::getColumnListing($this->table);

            foreach ($columnList as $c => $d) {
                if ($d == 'password') {
                    unset($columnList[$c]);
                }
                if ($d == 'id') {
                    unset($columnList[$c]);
                }
                if ($d == 'api_token') {
                    unset($columnList[$c]);
                }
            }

            if (null == $id) {
                $input['api_token'] = Str::random('20');
                $columnList         = array_merge($columnList, [
                    'api_token',
                ]);
            }
            foreach ($columnList as $field) {
                if (in_array($field, array_keys($input))) {
                    $data->$field = $input[$field];
                }
            }

            /**
             * if password is not empty, it will update the password
             */
            if (!empty($input['password'])) {
                $data->password = bcrypt($input['password']);
            }

            /**
             * If you want to reset the API_token
             * add paramenter
             * reset_token = true
             */
            if (!empty($input['reset_token'])) {
                if ($input['reset_token'] == 'true') {
                    $data->api_token = Str::random('20');
                }
            }

            /**
             * save the data
             */
            $data->save();

            // Auth::loginUsingId($data->id);
            // $user = array_merge(\Auth::user()->toArray(), [
            // 'api_token' => Auth::user()->api_token,
            // ]);
            // if (null == $id) {
            //     Mail::send('emails.register', ['user' => $user], function ($m) use ($user) {
            //         $m->to($user['email'], $user['email'])->subject('Greetings from WRP!');
            //     });
            // }
            return [
                'status' => 1,
                'data'   => $data,
            ];
        }
    }
}
