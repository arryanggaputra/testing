<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class Site extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'sites';

    /**
     * Create or Update Database
     *
     * @param  primary key $id if there's no $id this method will create new Data, if preset will be Update the Database
     */
    protected function manageData($id = null, $input = null)
    {
        /**
         * Check $id
         */
        if (null !== $id) {

            $getItem = $this->find($id);
            if (empty($getItem)) {
                return $this->result->make();
            }
        }

        if (empty($input)) {
            $input = Request::all();
        }

        $currentId = (null == $id) ? '' : $id;
        $required  = (null == $id) ? 'required' : '';

        /**
         * set validation on here
         * @var array
         */
        $customrules = [
            'name' => 'required|max:200',
            'url'  => 'required',
        ];

        // Create validation include custom rules
        $validation = Validator::make($input, $customrules);

        if ($validation->fails()) {
            $errors = $validation->messages()->all(':message');
            return [
                'status' => 0,
                'data'   => $errors,
            ];
        } else {
            $data       = (null == $id) ? new $this : $getItem;
            $columnList = Schema::getColumnListing($this->table);

            foreach ($columnList as $field) {
                if (in_array($field, array_keys($input))) {
                    $data->$field = $input[$field];
                }
            }

            /**
             * save the data
             */
            $data->save();

            return [
                'status' => 1,
                'data'   => $data,
            ];
        }
    }
}
