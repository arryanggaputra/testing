<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
 */

$factory->define(App\Models\User::class, function (Faker\Generator $faker) use ($factory) {
    return [
        'name'           => $faker->name,
        'email'          => $faker->email,
        'password'       => bcrypt('testing'),
        'type'           => 1,
        'api_token'      => str_random(20),
        'remember_token' => str_random(10),
    ];
});
$factory->defineAs(App\Models\User::class, 'admin', function (Faker\Generator $faker) use ($factory) {
    $user = $factory->raw(App\Models\User::class);
    return array_merge($user, ['type' => 2]);
});
$factory->defineAs(App\Models\User::class, 'user', function (Faker\Generator $faker) use ($factory) {
    $user = $factory->raw(App\Models\User::class);
    return array_merge($user, ['type' => 3]);
});
