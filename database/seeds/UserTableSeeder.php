<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = factory(App\Models\User::class, 3)->create();
        $users = factory(App\Models\User::class, 'admin', 3)->create();
        $users = factory(App\Models\User::class, 'user', 4)->create();
    }
}
