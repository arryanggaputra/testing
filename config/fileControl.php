<?php

return array(
    'AdminControllers' => array(
        'users'       => 'UsersController',
        'sites'       => 'SitesController',
        'admins'      => 'AdminsController',
        'superadmins' => 'SuperadminsController',
    ),
);
