$(window).load(function() {

	// make modal to center of page
	$('.modal').on('show.bs.modal', centerModals);
	$(window).on('resize', centerModals);
	$('.slideHome').backstretch("images/slide.png");
});

function centerModals(){
	$('.modal').each(function(i){
		var $clone = $(this).clone().css('display', 'block').appendTo('body');
		var top = Math.round(($clone.height() - $clone.find('.modal-content').height()) / 2);
		top = top > 0 ? top : 0;
		$clone.remove();
		$(this).find('.modal-content').animate({"margin-top":top});
	});
}