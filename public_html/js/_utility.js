$(function () {
	var _deleteData = $('.deleteData');
	_deleteData.click(function (e) {
		e.preventDefault();
		if (confirm('Your data will be permanently deleted, sure?')) {
			window.location = $(this).attr('data-href');
		}
	})
})

