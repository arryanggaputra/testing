**Authentication**
----
  Melakukan Authentication terhadap API untuk mendapatkan API_token .

* **URL**

  /authenticate

* **Method:**

  `POST`

*  **Data Params**

   **Required:**

	* `email`
	* `password`

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** `{"status": "success", "code": 200, "message": "200 OK", "data": {"..."} } `

* **Error Response:**

  * **Code:** 400 BAD REQUEST <br />
    **Content:** `{"status": "error", "code": 400, "message": "400 Bad Request", "data": ["The email field is required.", ] }`

  OR

  * **Code:** 401 <br />
    **Content:** `{"status": "error", "code": 401, "message": "401 Unauthorized", "data": null }`
