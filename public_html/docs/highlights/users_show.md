**Menampilkan Sebuah Kota**
----
  Menampilkan data sebuah User dengan id yang sesuai dengan API_token.

* **URL**

  /users/{id}

* **Method:**

  `GET`

*  **URL Params**

   **Required:**

   `id=[integer]`

*  **HEADER Params**

   **Required:**

   `X-Auth-Token`

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** `{"status": "success", "code": 200, "message": "200 OK", "data": {"id": "1101", "province_id": "11", "name": "Kab. Simeulue"} } `

* **Error Response:**

  * **Code:** 404 NOT FOUND <br />
    **Content:** `{"status": "error", "code": "404", "message": "404 Not Found", "data": null }`

  OR

  * **Code:** 401 <br />
    **Content:** `{"status": "error", "code": 401, "message": "401 Unauthorized", "data": null }`
