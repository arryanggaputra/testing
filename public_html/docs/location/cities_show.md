**Menampilkan Sebuah Kota**
----
  Menampilkan satu nama Kota .

* **URL**

  /location/cities/{id}

* **Method:**

  `GET`

*  **URL Params**

   **Required:**

   `id=[integer]`

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** `{"status": "success", "code": 200, "message": "200 OK", "data": {"id": "1101", "province_id": "11", "name": "Kab. Simeulue"} } `

* **Error Response:**

  * **Code:** 404 NOT FOUND <br />
    **Content:** `{"status": "error", "code": "404", "message": "404 Not Found", "data": null }`
