**Menampilkan Provinsi**
----
  Menampilkan daftar JSON seluruh provinsi di Indonesia.

* **URL**

  /location/provinces

* **Method:**

  `GET`

* **Parameter**

  None

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** `{   "status": "success", "code": 200, "message": "200 OK", "data": [{"id": "11", "name": "Aceh"}, ... ] }`